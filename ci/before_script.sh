#!/usr/bin/env bash

set -e
set -x
mkdir -p /root/.cache/unity3d
mkdir -p /root/.local/share/unity3d/Unity/
set +x

export UNITY_LICENSE_CONTENT=$(cat ../Unity_v2017.x.ulf)
echo $UNITY_LICENSE_CONTENT
cp ../Unity_v2017.x.ulf /root/.local/share/unity3d/Unity/Unity_lic.ulf

echo 'Writing $UNITY_LICENSE_CONTENT to license file /root/.local/share/unity3d/Unity/Unity_lic.ulf'
echo "$UNITY_LICENSE_CONTENT" | tr -d '\r\n' > /root/.local/share/unity3d/Unity/Unity_lic.ulf
