#!/usr/bin/env bash

set -e

docker run \
  -e BUILD_NAME \
  -e UNITY_LICENSE_CONTENT \
  -e BUILD_TARGET \
  -e UNITY_USERNAME \
  -e UNITY_PASSWORD \
  -w /project/myproject/unity3d-gitlab-ci-example  \
  -v $(pwd):/project/myproject/unity3d-gitlab-ci-example \
  $IMAGE_NAME \
  /bin/bash -c "/project/myproject/unity3d-gitlab-ci-example/ci/before_script.sh && /project/myproject/unity3d-gitlab-ci-example/ci/build.sh"