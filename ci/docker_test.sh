#!/usr/bin/env bash

set -e

docker run \
  -e UNITY_LICENSE_CONTENT \
  -e TEST_PLATFORM \
  -e UNITY_USERNAME \
  -e UNITY_PASSWORD \
  -w /project/myproject/unity3d-gitlab-ci-example \
  -v $(pwd):/project/myproject/unity3d-gitlab-ci-example \
  $IMAGE_NAME \
  /bin/bash -c "/project/myproject/unity3d-gitlab-ci-example/ci/before_script.sh && /project/myproject/unity3d-gitlab-ci-example/ci/test.sh"